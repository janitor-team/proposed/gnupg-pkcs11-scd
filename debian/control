Source: gnupg-pkcs11-scd
Section: utils
Priority: optional
Maintainer: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13)
	, libassuan-dev
	, libgpg-error-dev
	, libgcrypt20-dev
	, libpkcs11-helper-dev
	, libssl-dev
	, pkg-config
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/alteholz/gnupg-pkcs11-scd
Vcs-Git: https://salsa.debian.org/alteholz/gnupg-pkcs11-scd.git
Homepage: http://gnupg-pkcs11.sourceforge.net/
Rules-Requires-Root: no

Package: gnupg-pkcs11-scd
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: gnupg2 | gpgsm | gnupg
Description: GnuPG smart-card daemon with PKCS#11 support
 gnupg-pkcs11-scd is a drop-in replacement for the smart-card daemon (scd)
 shipped with GnuPG. The daemon interfaces to smart-cards by using RSA Security
 Inc. PKCS#11 Cryptographic Token Interface (Cryptoki).

Package: gnupg-pkcs11-scd-proxy
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, adduser
Suggests: gnupg2 | gpgsm | gnupg
Description: GnuPG smart-card daemon with PKCS#11 support, proxy
 gnupg-pkcs11-scd is a drop-in replacement for the smart-card daemon (scd)
 shipped with GnuPG. The daemon interfaces to smart-cards by using RSA Security
 Inc. PKCS#11 Cryptographic Token Interface (Cryptoki).
 .
 The daemon is a proxy into gnupg-pkcs11-scd-proxy-server which can be
 run within different security context to load the gnupg-pkcs11-scd
 smart-card daemon.
